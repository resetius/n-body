#include <stdio.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>

#include "n-body.h"

static void do_drawing(cairo_t *);

struct point {
    double x;
    double y;
};

struct {
    FILE* f;
    struct point* cur;
    struct point* prev;
    int count;

    struct point min;
    struct point max;

    GtkWidget *window;
    GtkWidget *darea;
} glob;

static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr,
                              gpointer user_data)
{
    do_drawing(cr);

    return FALSE;
}

double min(double a, double b) {
    return a < b ? a : b;
}

static void do_drawing(cairo_t *cr)
{
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_set_line_width(cr, 0.5);

//    printf("draw\n");

    GtkAllocation rect;
    gtk_widget_get_allocation(glob.darea, &rect);
/*
    rect.x;
    rect.y;
    rect.width;
    rect.height;
*/
    double scale = min((rect.width - 20) / (glob.min.x - glob.min.x), (rect.height - 20) / (glob.max.y - glob.min.y));

    int i, j;
    for (i = 0; i < glob.count; i++ ) {
        // cairo_move_to(cr, glob.coordx[i], glob.coordy[i]);
        // cairo_line_to(cr, glob.coordx[j], glob.coordy[j]);
        struct point* p = &glob.cur[i];

        double x = p->x;
        double y = p->y;

        // printf("draw %lf %lf\n", p->x, p->y);

        // cairo_arc(cr, p->x, p->y, 1, 0, 2*M_PI);

        x -= glob.min.x;
        y -= glob.min.y;
        x *= scale;
        y *= scale;

        cairo_move_to(cr, x, y);
        cairo_arc(cr, x, y, 0.5, 0, 2*M_PI);
    }

    glob.count = 0;
    cairo_stroke(cr);
}

int on_timeout(void* timeout) {
    // printf("timeout\n");

    char* delim = " ";
    char buf[4096];
    fgets(buf, sizeof(buf), glob.f);

    int count = 0;

    struct point min;
    struct point max;

    min = glob.min;
    max = glob.max;

    for (char* tok = strtok(buf, delim); tok; ) {
        double x, y, v;
        x = atof(tok);
        tok = strtok(0, delim);
        if (!tok) break;
        y = atof(tok);
        tok = strtok(0, delim);
        if (!tok) break;
        v = atof(tok);
        tok = strtok(0, delim);
        if (!tok) break;

        if (fabs(x) > max.x) {
            max.x = fabs(x);
            min.x = -max.x;
        }
        if (fabs(y) > max.y) {
            max.y = fabs(y);
            min.y = -max.y;
        }

        glob.cur[count].x = x;
        glob.cur[count].y = y;

        count += 1;
    }

    glob.max = max;
    glob.min = min;
    glob.count = count;
    struct point* tmp = glob.cur;
    glob.cur = glob.prev;
    glob.prev = tmp;

//    GtkAllocation rect;
//    gtk_widget_get_allocation(glob.darea, &rect);
    gtk_widget_queue_draw(glob.window);

    g_timeout_add(5, on_timeout, 0);
    return 0;
}

int main(int argc, char *argv[])
{
    GtkWidget *window;
    GtkWidget *darea;

    glob.f = stdin;
    glob.count = 0;
    glob.cur = malloc(sizeof(struct point)*100);
    glob.prev = malloc(sizeof(struct point)*100);

    glob.min.x = DBL_MAX;
    glob.min.y = DBL_MAX;
    glob.max.x = DBL_MIN;
    glob.max.y = DBL_MIN;

    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    darea = gtk_drawing_area_new();

    glob.window = window;
    glob.darea = darea;

    gtk_container_add(GTK_CONTAINER(window), darea);

    gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK);

    g_signal_connect(G_OBJECT(darea), "draw",
                     G_CALLBACK(on_draw_event), NULL);
    g_signal_connect(window, "destroy",
                     G_CALLBACK(gtk_main_quit), NULL);

    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);
    gtk_window_set_title(GTK_WINDOW(window), "Lines");

    g_timeout_add(100, on_timeout, 0);

    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
