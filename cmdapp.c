#include <stdio.h>
#include <string.h>
#include <math.h>

#include "n-body.h"

// config example
// runge4
// 0.01
// 2
// 0 0  0 0  0 0 1000 1
// 0 10 0 10 0 0 1 0
// # x y z vx vy vz mass fixed
int main(int argc, char** argv)
{
    int n;
    double dt = 0.01;
    struct model* m;
    char method[256];
    model_step_t step;
    FILE * f;
    double t, T;
    double G;

    if (argc < 2) {
        fprintf(stderr, "config file requered\n");
        return -1;
    }

    f = fopen(argv[1], "rb");
    if (!f) {
        fprintf(stderr, "cannot open config file '%s'\n", argv[1]);
    }

    if (fscanf(f, "%255s", method) != 1) {
        fprintf(stderr, "cannot fscanf method\n");
        return -1;
    }

    if (!strcmp(method, "runge4")) {
        step = model_step_runge4;
    } else if (!strcmp(method, "naive")) {
        step = model_step_naive;
    } else {
        fprintf(stderr, "unknown method '%s'\n", method);
        return -1;
    }

    if (fscanf(f, "%lf", &dt) != 1) {
        fprintf(stderr, "cannot fscanf dt\n");
        return -1;
    };

    if (fscanf(f, "%lf", &T) != 1) {
        fprintf(stderr, "cannot fscanf T\n");
        return -1;
    };

    if (fscanf(f, "%lf", &G) != 1) {
        fprintf(stderr, "cannot fscanf G\n");
        return -1;
    };

    if (fscanf(f, "%d", &n) != 1) {
        fprintf(stderr, "cannot fscanf n\n");
        return -1;
    }

    m = model_new(n, dt, G);

    for (int i = 0; i < n; ++i) {
        double r[3];
        double v[3];
        double mass;
        int fixed;

        if (fscanf(f, "%lf%lf%lf%lf%lf%lf%lf%d", &r[0], &r[1], &r[2], &v[0], &v[1], &v[2], &mass, &fixed) != 8) {
            fprintf(stderr, "bad config format\n");
            return -1;
        }
        printf("add body: %lf %lf %lf %lf %lf %lf %lf %d\n", r[0], r[1], r[2], v[0], v[1], v[2], mass, fixed);

        if (model_body_add(m, r, v, mass, fixed) != 0) {
            fprintf(stderr, "cannot add body\n");
            return -1;
        }
    }

    while (t < T) {
        model_print(m);
        step(m);
        t += dt;
    }

    model_print(m);
    model_free(m);
}
