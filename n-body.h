#pragma once

struct model;

struct model* model_new(int capacity, double dt, double G);
void model_free(struct model*);
int model_body_add(struct model* model, double* r, double* v, double m, int fixed);
void model_step_naive(struct model* model);
void model_step_runge4(struct model* model);
void model_print(struct model* model);

typedef void (*model_step_t)(struct model* );