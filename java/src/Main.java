import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.*;

class Point implements Cloneable {
    public double x;
    public double y;
    public double vx;
    public double vy;
    public double m;

    public Point(double x, double y, double vx, double vy, double m) {
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
        this.m = m;
    }

    public Point setFixed(boolean f) {
        fixed = f;
        return this;
    }

    @Override
    public Point clone() throws CloneNotSupportedException {
        super.clone();
        return new Point(x, y, vx, vy, m).setFixed(fixed);
    }

    boolean fixed = false;
}

class Visible extends JPanel
{
    double minx;
    double maxx;
    double miny;
    double maxy;

    ArrayList<Point> points = new ArrayList<>();

    public void setPoints(Point[] points) {
        synchronized (this.points) {
            if (this.points.isEmpty()) {
                minx = points[0].x;
                maxx = points[0].x;
                miny = points[0].y;
                maxy = points[0].y;


                for (Point p : points) {
                    if (p.fixed) {
                        this.points.add(p);
                    }
                }
            }
        }

        for (Point p : points) {
            if (p.x > maxx) {
                maxx = p.x;
            }
            if (p.x < minx) {
                minx = p.x;
            }
            if (p.y > maxy) {
                maxy = p.y;
            }
            if (p.y < miny) {
                miny = p.y;
            }

            if (!p.fixed) {
                synchronized (this.points) {
                    this.points.add(p);
                }
            }
        }
    }

    void paintPoints(Graphics g) {
        double X = getWidth();
        double Y = getHeight();

        g.clearRect(0, 0, (int)X, (int)Y);

        double scale = Math.min((X - 20) / (maxx - minx), (Y - 20) / (maxy - miny));
        int offX = X > Y ? (int)(X - Y) / 2 : 10;
        int offY = X > Y ? 10 : (int)(Y - X) / 2;
        synchronized (this.points) {
            for (Point p : points) {
                double x = p.x;
                double y = p.y;
                x -= minx;
                y -= miny;

                x *= scale;
                y *= scale;

                //System.out.printf("%f %f\n", x, y);

                g.setColor(Color.blue);
                g.drawOval((int) (x + offX), (int) (y + offY), 10, 10);
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        paintPoints(g);
    }
}

class Model
{
    Point[] points;
    Point[] newPoints;
    private final double dt = 0.1;

    public Model(Point[] points) {
        this.points = points;
        this.newPoints = new Point[points.length];

        for (int i = 0; i < points.length; ++i) {
            newPoints[i] = new Point(0, 0, 0, 0, 0);
        }
    }

    private Point[] f(Point [] points) {
        Point [] ret = new Point[points.length];
        for (int i = 0; i < points.length; ++i) {
            Point p = points[i];
            if (p.fixed) {
                ret[i] = p;
            }

            Point pp = new Point(p.vx, p.vy, 0, 0, p.m);

            for (int j = 0; j < points.length; ++j) {
                if (i == j) {
                    continue;
                }

                Point p1 = points[j];

                double r = (p.x - p1.x) * (p.x - p1.x) +
                        (p.y - p1.y) * (p.y - p1.y);
                r = Math.sqrt(r);

                pp.vx += p1.m * (p1.x - p.x) / r / r / r;
                pp.vy += p1.m * (p1.y - p.y) / r / r / r;
            }

            ret[i] = pp;
        }

        return ret;
    }

    private void euler(Point [] points) {
        Point [] ff = f(points);

        for (int i = 0; i < points.length; ++i) {
            Point p = points[i];
            if (p.fixed) {
                continue;
            }

            p.vx += ff[i].vx * dt;
            p.vy += ff[i].vy * dt;

            p.x += ff[i].x * dt;
            p.y += ff[i].y * dt;
        }
    }

    private void runge4(Point [] points) {
        Point[] k1 = f(points);
        Point[] k2 = new Point[points.length];
        for (int i = 0; i < points.length; ++i) {
            Point p = points[i];
            if (p.fixed) {
                k2[i] = p;
                continue;
            }
            k2[i] = new Point(0, 0, 0, 0, k1[i].m);
            k2[i].vx = p.vx + 0.5 * dt * k1[i].vx;
            k2[i].vy = p.vy + 0.5 * dt * k1[i].vy;
            k2[i].x = p.x + 0.5 * dt * k1[i].x;
            k2[i].y = p.y + 0.5 * dt * k1[i].y;
        }
        k2 = f(k2);

        Point[] k3 = new Point[points.length];
        for (int i = 0; i < points.length; ++i) {
            Point p = points[i];
            if (p.fixed) {
                k3[i] = p;
                continue;
            }
            k3[i] = new Point(0, 0, 0, 0, k2[i].m);
            k3[i].vx = p.vx + 0.5 * dt * k2[i].vx;
            k3[i].vy = p.vy + 0.5 * dt * k2[i].vy;
            k3[i].x = p.x + 0.5 * dt * k2[i].x;
            k3[i].y = p.y + 0.5 * dt * k2[i].y;
        }
        k3 = f(k3);

        Point[] k4 = new Point[points.length];
        for (int i = 0; i < points.length; ++i) {
            Point p = points[i];
            if (p.fixed) {
                k4[i] = p;
                continue;
            }
            k4[i] = new Point(0, 0, 0, 0, k3[i].m);
            k4[i].vx = p.vx + dt * k3[i].vx;
            k4[i].vy = p.vy + dt * k3[i].vy;
            k4[i].x = p.x + dt * k3[i].x;
            k4[i].y = p.y + dt * k3[i].y;
        }
        k4 = f(k4);

        for (int i = 0; i < points.length; ++i) {
            Point p = points[i];
            if (p.fixed) {
                continue;
            }

            p.vx += 1.0 / 6.0 * dt * (k1[i].vx + 2.0*k2[i].vx + 2.0*k3[i].vx + k4[i].vx);
            p.vy += 1.0 / 6.0 * dt * (k1[i].vy + 2.0*k2[i].vy + 2.0*k3[i].vy + k4[i].vy);

            p.x += 1.0 / 6.0 * dt * (k1[i].x + 2.0*k2[i].x + 2.0*k3[i].x + k4[i].x);
            p.y += 1.0 / 6.0 * dt * (k1[i].y + 2.0*k2[i].y + 2.0*k3[i].y + k4[i].y);

            System.out.printf("%d %f %f %.16f\n", i, p.x, p.y, Math.sqrt(p.vx*p.vx+p.vy*p.vy));
        }
    }

    public void calc() {
        /*
        double dt = 0.1;
        int i = 0;
        for (Point p : points) {
            if (p.fixed) {
                newPoints[i++] = p;
                continue;
            }

            double gx = 0;
            double gy = 0;

            for (Point p1 : points) {
                if (p1 == p) {
                    continue;
                }

                double r = (p.x - p1.x) * (p.x - p1.x) +
                        (p.y - p1.y) * (p.y - p1.y);
                r = Math.sqrt(r);

                gx += p1.m * (p1.x - p.x) / r / r / r;
                gy += p1.m * (p1.y - p.y) / r / r / r;
            }

            double vx = p.vx + gx * dt;
            double vy = p.vy + gy * dt;

            double x = p.x + vx * dt;
            double y = p.y + vy * dt;

            Point newPoint = newPoints[i++];
            newPoint.x = x;
            newPoint.y = y;
            newPoint.vx = vx;
            newPoint.vy = vy;
            newPoint.m = p.m;

            System.out.printf("%f %f %f\n", x, y, Math.sqrt(vx*vx+vy*vy));

            newPoint.fixed = p.fixed;
        }

        for (int j = 0; j < points.length; ++j) {
            points[j] = newPoints[j];
        }
        */

        //euler(points);
        runge4(points);
    }

    public Point[] getPoints() {
        try {
            Point[] pp = new Point[points.length];
            for (int i = 0; i < points.length; ++i) {
                pp[i] = points[i].clone();
            }

            return pp;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}

class Calc implements Runnable {
    final Model model;
    final Visible v;
    public Calc(Model m, Visible v) {
        this.model = m;
        this.v = v;
    }

    @Override
    public void run() {
        while (true) {
            model.calc();
            v.setPoints(model.getPoints());
            v.repaint();
            try {
                Thread.sleep(50);
            } catch (Throwable e) {
            }
        }
    }
}

public class Main {
    public static void main(String [] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Draw Line");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBackground(Color.white);
        frame.setSize(2000, 1500);

        Visible panel = new Visible();

        frame.add(panel);

        Point [] points = new Point[2];
        points[0] = new Point(0, 0, 0, 0, 1000).setFixed(true);
        points[1] = new Point(0, 10, Math.sqrt(1000./10.), 0, 1);
        //points[2] = new Point(0, 5, 10, 0, 1);
        //points[3] = new Point(0, 6, 0.1, 0, 0.001);

        Model m = new Model(points);
        Calc c = new Calc(m, panel);
        Thread t = new Thread(c);
        t.start();

        frame.setVisible(true);
    }
}
